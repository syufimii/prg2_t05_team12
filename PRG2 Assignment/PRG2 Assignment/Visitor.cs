﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }

        public string Nationality { get; set; }

        public Visitor(string n, string pn, string na)
        {
            Name = n;
            PassportNo = pn;
            Nationality = na;
        }

        public override double CalculateSHNCharges()
        {
            double charges = 200;

            TravelEntry te;

            foreach (TravelEntry t in base.TravelEntryList)
            {
                if (t.ShnEndDate < DateTime.Now && t.IsPaid == false)
                {
                    te = t;

                    string lastCountryOfEmbarkation = te.LastCountryOfEmbarkation;

                    if (lastCountryOfEmbarkation == "New Zealand" || lastCountryOfEmbarkation == "Vietnam")
                    {
                        return (charges + 80) * 1.07;
                    }
                    else if (lastCountryOfEmbarkation == "Macao")
                    {
                        return (charges + 80) * 1.07;
                    }
                    else
                    {
                        SHNFacility shn = te.ShnStay;
                        return (charges + 2000) * 1.07 + shn.CalculateTravelCost(te.EntryMode, te.EntryDate);
                    }
                }
            }

            return 0;
        }

        public override string ToString()
        {
            return "Name: "+ Name + "\t\tPassport No: " + PassportNo + "   \t\tNationality: " + Nationality;
        }
    }
}
