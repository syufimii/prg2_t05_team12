﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class TraceTogetherToken
    {
        public string SerialNo { get; set; }

        public string CollectionLocation { get; set; }

        public DateTime ExpiryDate { get; set; }

        public TraceTogetherToken() { }

        public TraceTogetherToken(string sn, string cl, DateTime ed)
        {
            SerialNo = sn;
            CollectionLocation = cl;
            ExpiryDate = ed;
        }

        public bool IsEligibleForReplacement()
        {
            bool IsEligible = false;

            if (DateTime.Now.Month >= ExpiryDate.Month )
            {
                IsEligible = true;
            }
            return IsEligible;
        }

        public void ReplaceToken(string sn, string cl)
        {
            SerialNo = sn;
            CollectionLocation = cl;
        }

        public override string ToString()
        {
            return "\nSerial No: " + SerialNo + "\tCollection Location: " + CollectionLocation + "     \tExpiry Date: " + ExpiryDate.ToString("dd/MM/yyyy");
        }
    }


}
