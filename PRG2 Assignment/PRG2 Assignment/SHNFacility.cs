﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class SHNFacility
    {
        public string FacilityName { get; set; }

        public int FacilityCapacity { get; set; }

        public int FacilityVacancy { get; set; }

        public double DistFromAirCheckpoint { get; set; }

        public double DistFromSeaCheckpoint { get; set; }

        public double DistFromLandCheckpoint { get; set; }

        public SHNFacility () { }

        public SHNFacility (string fn, int fc, double air, double sea, double land)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            FacilityVacancy = fc; // facility would be empty when added so vacancy = capacity
            DistFromAirCheckpoint = air;
            DistFromSeaCheckpoint = sea;
            DistFromLandCheckpoint = land;
        }

        public double CalculateTravelCost(string em, DateTime ed)
        {
            double fare = 50;

            if (em == "Air")
            {
                fare += 0.22 * DistFromAirCheckpoint;
            }
            else if (em == "Sea")
            {
                fare += 0.22 * DistFromSeaCheckpoint;
            }
            else if (em == "Land")
            {
                fare += 0.22 * DistFromLandCheckpoint;
            }

            TimeSpan time1 = new TimeSpan(0, 0, 0);
            TimeSpan time2 = new TimeSpan(5, 59, 0); 
            TimeSpan time3 = new TimeSpan(18, 0, 0);
            TimeSpan time4 = new TimeSpan(8, 59, 0);

            if (time1 <= ed.TimeOfDay && ed.TimeOfDay <= time2)
            {
                fare *= 1.5;
            }
            else if (time3 <= ed.TimeOfDay && ed.TimeOfDay <= time4)
            {
                fare *= 1.25;
            }

            return fare * 1.07; // 7% gst
        }

        public bool IsAvaliable()
        {
            if (FacilityVacancy > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\tFacility Capacity" + FacilityCapacity + "\tFacility Vacancy" + FacilityVacancy + "\tDistance from Air Checkpoint" + DistFromAirCheckpoint + "\tDistance from Sea Checkpoint" + DistFromSeaCheckpoint + "\tDistance from Land Checkpoint" + DistFromLandCheckpoint;
        }
    }
}
