﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;


namespace PRG2_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            // === General ===
            // 2. Load SHN Facility Data (put in front to initalise the TravelEntry inside InitPersonLists)
            List<SHNFacility> facilityList = new List<SHNFacility>();
            InitFacilityList(facilityList);

            // 1.1 create persons list & business location list
            List<Person> personList = new List<Person>();

            List<BusinessLocation> businessList = new List<BusinessLocation>();
            
            // 1.2 initialize both lists
            InitBusinessList(businessList);
            InitPersonLists(personList, facilityList);

            string[] menu = { "List all Visitors", "List Person Details", "Assign/Replace TraceTogether Token" , "List all Business Location",
                              "Edit Business Location Capacity", "SafeEntry CheckIn", "SafeEntry CheckOut", "List All SHN Facilities", "Create Visitor", "Create TravelEntry Record", "Calculate SHN Charges"};

            bool isRunning = true;
            while (isRunning)
            {
                int tokenSerial = 23456;

                // === Menu ===
                Console.WriteLine("===============");
                for (int i = 0; i < menu.Length; i++)
                {
                    Console.WriteLine("{0}. {1}", i + 1, menu[i]);
                }

                Console.Write("===============\nEnter choice: ");
                string choice = Console.ReadLine();
                Console.WriteLine();

                // === Choices ===
                if (choice == "1")
                {
                    // 3. List all Visitors
                    DisplayPersonList(personList);
                }
                else if (choice == "2")
                {
                    // 4. List Person Details
                    Console.WriteLine("=== List Person Details ===");

                    // prompt user for name
                    Console.Write("Enter persons name: ");
                    string name = Console.ReadLine();

                    // search for person
                    Person searchPerson = SearchPerson(name, personList);

                    // list person details including TravelEntry and SafeEntry details
                    Console.WriteLine(searchPerson);

                    foreach (TravelEntry t in searchPerson.TravelEntryList)
                    {
                        Console.WriteLine(t.ToString());
                    }

                    foreach (SafeEntry s in searchPerson.SafeEntryList)
                    {
                        Console.WriteLine(s.ToString());
                    }
                }
                else if (choice == "3")
                {
                    // === SafeEntry/TraceTogether ===
                    // 5. Assign/Replace TraceTogether Token
                    Console.WriteLine("=== Assign/Replace TraceTogether Token ===");

                    // prompt user for name
                    Console.Write("Enter persons name: ");
                    string name = Console.ReadLine();

                    // search for resident name
                    Person searchResident = SearchPerson(name, personList);

                    // create and assign a TraceTogetherToken object if resident has no existing token
                    if (searchResident is Resident)
                    {
                        Resident resident = (Resident)searchResident;

                        string[] address = resident.Address.Split(" ");
                        string collectLocation = address[1] + " CC";
                        DateTime issueDate = DateTime.Now;

                        // if resident has no token, create token
                        if (resident.Token == null)
                        {
                            resident.Token = new TraceTogetherToken("T" + (tokenSerial + 1111), collectLocation, issueDate.AddMonths(6));
                        }
                        // if resident has token, check if token is elligible for replacement
                        else
                        {
                            if (resident.Token.IsEligibleForReplacement() == true)
                            {
                                resident.Token.ReplaceToken("T" + (tokenSerial + 1111), collectLocation);
                            }
                            else
                            {
                                Console.WriteLine("{0} is not eligible for replacement of token.", resident.Name);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("You are not allowed to be assigned with a TraceTogether Token.");
                    }
                    tokenSerial++;
                }
                else if (choice == "4")
                {
                    // 6. List all Business Location
                    DisplayBusinessList(businessList);
                }
                else if (choice == "5")
                {
                    // 7. Edit Business Location Capacity
                    // prompt user to enter detail
                    Console.Write("Enter Branch Code: ");
                    string detail = Console.ReadLine();

                    BusinessLocation location = SearchLocation(detail, businessList);

                    Console.Write("Enter new maximum capacity: ");
                    location.MaximumCapacity = Convert.ToInt32(Console.ReadLine());

                }
                else if (choice == "6")
                {
                    // 8. SafeEntry CheckIn
                    // prompt user for name
                    Console.Write("Enter name: ");
                    string name = Console.ReadLine();

                    // search for person
                    Person found = SearchPerson(name, personList);

                    // list all business locations
                    DisplayBusinessList(businessList);

                    // prompt user to select for business location to check -in
                    Console.Write("Select business location to check in (Branch Code): ");
                    string checkIn = Console.ReadLine();

                    BusinessLocation location = SearchLocation(checkIn, businessList);

                    // create SafeEntry object if the location is not full, and increase visitorsNow count
                    if (location.IsFull(location) == false)
                    {
                        // add SafeEntry object to person
                        found.AddSafeEntry(new SafeEntry(DateTime.Now, location));
                        location.VisitorsNow++;
                        Console.WriteLine("Successfully Checked In to {0}", location.BusinessName);

                    }
                    else
                    {
                        Console.WriteLine("Location is full right now. Please come again later.");
                    }
                }
                else if (choice == "7")
                {
                    // 9. SafeEntry CheckOut
                    // prompt user for name
                    Console.Write("Enter name: ");
                    string name = Console.ReadLine();

                    // search for person
                    Person found = SearchPerson(name, personList);

                    // list SafeEntry records for that person that have not been checked-out
                    int i = 1;
                    foreach (SafeEntry safeEntry in found.SafeEntryList)
                    {
                        Console.WriteLine("{0}. {1}", i, safeEntry);
                        i++;
                    }

                    // prompt user to select record to check -out
                    Console.Write("Select location to check out: ");
                    string checkout = Console.ReadLine();

                    BusinessLocation location = SearchLocation(checkout, businessList);
                    foreach (SafeEntry safeEntry in found.SafeEntryList)
                    {
                        if (safeEntry.Location == location)
                        {
                            safeEntry.PerformCheckOut();
                            location.VisitorsNow--;
                        }
                    }


                    // call PerformCheckOut() to check -out, and reduce visitorsNow by 1
                }
                // === TravelEntry ===

                else if (choice == "8")
                {
                    // 10. List All SHN Facilities
                    foreach(SHNFacility f in facilityList)
                    {
                        Console.WriteLine(f.ToString());
                    }
                }
                else if (choice == "9")
                {
                    // 11. Create Visitor

                    // prompt user for details
                    Console.Write("Enter Visitor's Name: ");
                    string vName = Console.ReadLine();
                    Console.Write("Enter Visitor's PassportNo: ");
                    string vPassNo = Console.ReadLine();
                    Console.Write("Enter Visitor's Nationality: ");
                    string vNation = Console.ReadLine();

                    // create Visitor object
                    Visitor v = new Visitor(vName, vPassNo, vNation);

                    personList.Add(v);
                }
                else if (choice == "10")
                {
                    // 12. Create TravelEntry Record

                    // prompt user for name
                    Console.Write("Enter Name: ");
                    string traName = Console.ReadLine();

                    // search for person
                    Person traPerson = SearchPerson(traName, personList);

                    // prompt user for details
                    Console.Write("Enter Last Country of Embarkation: ");
                    string traEmbark = Console.ReadLine();
                    Console.Write("Enter Entry Mode: ");
                    string traMode = Console.ReadLine();

                    // create TravelEntry object
                    TravelEntry tra = new TravelEntry(traEmbark, traMode, DateTime.Now);

                    // call CalculateSHNDuration() to calculate SHNEndDate based on criteria given in the background brief
                    tra.CalculateSHNDuration();

                    if (traEmbark != "New Zealand" && traEmbark != "Vietnam")
                    {
                        Console.WriteLine("SHN Facilities: ");
                        foreach (SHNFacility f in facilityList)
                        {
                            Console.WriteLine(f.ToString());
                        }
                        Console.WriteLine("Choose one (enter name): ");
                        string traSHN = Console.ReadLine();
                        foreach (SHNFacility f in facilityList)
                        {
                            if (f.FacilityName == traSHN)
                            {
                                tra.AssignSHNFacility(f); // vacancy count is reduced in the command
                            }
                        }
                     }

                        traPerson.AddTravelEntry(tra);
                }
                else if (choice == "11")
                {
                    // 13. Calculate SHNCharges
                    Console.Write("Enter Name: ");
                    string shnName = Console.ReadLine();

                    Person shnPerson = SearchPerson(shnName, personList);
                    double shnCost = shnPerson.CalculateSHNCharges(); //searching for Travel Entry included in the command

                    Console.Write("Please pay ${0} for the SHN Charges.", shnCost);

                    foreach (TravelEntry t in shnPerson.TravelEntryList)
                    {
                        if (t.ShnEndDate < DateTime.Now && t.IsPaid == false)
                        {
                            t.IsPaid = true;
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Please input a number from 1 to 11!!");
                }
            }

        }
        static void InitPersonLists(List<Person> pList, List<SHNFacility> facilityList)
        {
            StreamReader sr = new StreamReader("Person.csv");
            string line = sr.ReadLine();
            line = sr.ReadLine();
            while(line != null)
            {

                string[] splitLine = line.Split(",");
                if (splitLine[0] == "visitor")
                {
                    Visitor info = new Visitor(splitLine[1], splitLine[4], splitLine[5]);
                    // adding Visitor:person into Persons lists
                    pList.Add(info);
                    //TravelEntry initTravelEntry = new TravelEntry(splitLine[9], splitLine[10], Convert.ToDateTime(splitLine[11])); // COMMENTED OUT AS Converting To DateTime has an error
                    //initTravelEntry.ShnEndDate = Convert.ToDateTime(splitLine[12]);
                    //initTravelEntry.IsPaid = Convert.ToBoolean(splitLine[13]);
                    //foreach(SHNFacility f in facilityList)
                    //{
                    //    if (f.FacilityName == splitLine[14])
                    //    {
                    //        initTravelEntry.AssignSHNFacility(f);
                    //    }
                    //}
                    //info.AddTravelEntry(initTravelEntry);
                }
                else if (splitLine[0] == "resident")
                {
                    // adding Resident:person into Persons lists
                    Resident info = new Resident(splitLine[1], splitLine[2], Convert.ToDateTime(splitLine[3]));
                    // if resident does not have token
                    if (splitLine[6] != "")
                    {
                        TraceTogetherToken token = new TraceTogetherToken(splitLine[6], splitLine[7], Convert.ToDateTime(splitLine[8]));
                        info.Token = token;
                    }
                    pList.Add(info);
                    //TravelEntry initTravelEntry = new TravelEntry(splitLine[9], splitLine[10], Convert.ToDateTime(splitLine[11]));
                    //initTravelEntry.ShnEndDate = Convert.ToDateTime(splitLine[12]);
                    //initTravelEntry.IsPaid = Convert.ToBoolean(splitLine[13]);
                    //foreach (SHNFacility f in facilityList)
                    //{
                    //    if (f.FacilityName == splitLine[14])
                    //    {
                    //        initTravelEntry.AssignSHNFacility(f);
                    //    }
                    //}
                    //info.AddTravelEntry(initTravelEntry);
                }


                line = sr.ReadLine();
            }
            sr.Close();
        }

        static void InitBusinessList(List<BusinessLocation> bList)
        {
            StreamReader sr = new StreamReader("BusinessLocation.csv");
            string line = sr.ReadLine();
            line = sr.ReadLine();
            while (line != null)
            {
                string[] splitLine = line.Split(",");
                bList.Add(new BusinessLocation(splitLine[0], splitLine[1], Convert.ToInt32(splitLine[2]), 0));
                line = sr.ReadLine();
            }
            sr.Close();
        }

        static void InitFacilityList(List<SHNFacility> facilityList)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();

                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    string data = readTask.Result;
                    facilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                }
            }
        }

        static void DisplayPersonList(List<Person> pList)
        {
            foreach(Person p in pList)
            {
                Console.WriteLine(p.ToString());
            }
        }

        static void DisplayBusinessList(List<BusinessLocation> bList)
        {
            foreach(BusinessLocation bl in bList)
            {
                Console.WriteLine(bl.ToString());
            }
        }

        static Person SearchPerson(string name, List<Person> pList)
        {
            Person found = null;
            foreach (Person p in pList)
            {
                if (name == p.Name)
                {
                    found = p;
                }
            }
            return found;
        }

        static BusinessLocation SearchLocation(string code, List<BusinessLocation> bList)
        {
            BusinessLocation found = null;
            foreach (BusinessLocation bl in bList)
            {
                if (code == bl.BranchCode)
                {
                    found = bl;
                }
            }
            return found;
        }
    }
}
