﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class SafeEntry
    {
        private DateTime checkIn;

        public DateTime CheckIn
        {
            get { return checkIn; }
            set { checkIn = value; }
        }
        private DateTime checkOut;

        public DateTime CheckOut
        {
            get { return checkOut; }
            set { checkOut = value; }
        }
        private BusinessLocation location;

        public BusinessLocation Location
        {
            get { return location; }
            set { location = value; }
        }
        public SafeEntry() { }
        public SafeEntry(DateTime ci, BusinessLocation l)
        {
            CheckIn = ci;
            Location = l;
        }
        public void PerformCheckOut()
        {
            DateTime checkoutTime = DateTime.Now;
            CheckOut = checkoutTime;
        }

        public override string ToString()
        {
            return "Check In: " + CheckIn +  "\n" + Location;
        }

    }
}
