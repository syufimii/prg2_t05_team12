﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class BusinessLocation
    {
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }

        public BusinessLocation() { }
        public BusinessLocation(string bn, string bc, int mc, int vn)
        {
            BusinessName = bn;
            BranchCode = bc;
            MaximumCapacity = mc;
            VisitorsNow = vn;
        }
        public bool IsFull(BusinessLocation bl)
        {
            if (bl.VisitorsNow >= bl.MaximumCapacity)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override string ToString()
        {
            return "Business Name: " + BusinessName + "  \t\tBranch Code: " + BranchCode + "\tMaximum Capacity: " + MaximumCapacity + "\tVisitors Now: " + VisitorsNow;
        }
    }
}
