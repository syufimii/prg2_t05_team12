﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    abstract class Person
    {
        public string Name { get; set; }
        public List<TravelEntry> TravelEntryList { get; set; }
        public List<SafeEntry> SafeEntryList { get; set; }
        public Person() 
        { 
            TravelEntryList = new List<TravelEntry>();
            SafeEntryList = new List<SafeEntry>(); 
        }
        public Person(string n)
        {
            Name = n;
            TravelEntryList = new List<TravelEntry>();
            SafeEntryList = new List<SafeEntry>();
        }
        public void AddTravelEntry(TravelEntry travelEntry)
        {
            TravelEntryList.Add(travelEntry);
        }
        public void AddSafeEntry(SafeEntry safeEntry)
        {
            SafeEntryList.Add(safeEntry);
        }
        public abstract double CalculateSHNCharges();

        public override string ToString()
        {

            return "Name: " + Name;
        }

    }
}
