﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class Resident : Person
    {
        public string Address { get; set; }

        public DateTime LastLeftCountry { get; set; }

        public TraceTogetherToken Token { get; set; }

        public Resident(string n, string a, DateTime l):base(n)
        {
            Address = a;
            LastLeftCountry = l;
        }

        public override double CalculateSHNCharges()
        {
            double charges = 200;

            TravelEntry te;

            foreach (TravelEntry t in base.TravelEntryList) {
                if (t.ShnEndDate < DateTime.Now && t.IsPaid == false)
                {
                    te = t;

                    string lastCountryOfEmbarkation = te.LastCountryOfEmbarkation;

                    if (lastCountryOfEmbarkation == "New Zealand" || lastCountryOfEmbarkation == "Vietnam")
                    {
                        return charges * 1.07;
                    }
                    else if (lastCountryOfEmbarkation == "Macao")
                    {
                        return (charges + 20) * 1.07;
                    }
                    else
                    {
                        return (charges + 20 + 1000) * 1.07;
                    }
                }
            }
            return 0;
        }

        public override string ToString()
        {
            return "Name: " + Name + "\t\tAddress: " + Address + "   \t\tLast Left Country: " + LastLeftCountry.ToString("dd/MM/yyyy") + Token;
        }


    }
}
