﻿//============================================================
// Student Number : S10208124, S10203293
// Student Name : Ahmad Syufi Mikail, Teo Wei Hao
// Module Group : P05
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_Assignment
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }

        public string EntryMode { get; set; }

        public DateTime EntryDate { get; set; }

        public DateTime ShnEndDate { get; set; }

        public SHNFacility ShnStay { get; set; }

        public bool IsPaid { get; set; }

        public TravelEntry() { }

        public TravelEntry(string lc, string em, DateTime ed)
        {
            LastCountryOfEmbarkation = lc;
            EntryMode = em;
            EntryDate = ed;
        }

        public void AssignSHNFacility(SHNFacility sf)
        {
            ShnStay = sf;
            sf.FacilityVacancy -= 1;
        }

        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                ShnEndDate = EntryDate;
            }
            else if (LastCountryOfEmbarkation == "Macao")
            {
                ShnEndDate = EntryDate.AddDays(7);
            }
            else
            {
                ShnEndDate = EntryDate.AddDays(14);
            }
        }

        public override string ToString()
        {
            return "Last Country of Embarkation: " + LastCountryOfEmbarkation + "\tEntry Mode: " + EntryMode + "\tEntry Date: " + EntryDate.ToString() + "\tIs Paid: " + IsPaid;
        }
    }
}
